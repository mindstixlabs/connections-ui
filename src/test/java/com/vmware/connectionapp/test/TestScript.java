/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.test;

import java.io.IOException;
import java.net.MalformedURLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.vmware.connectionapp.screens.BaseScreen;
import com.vmware.connectionapp.screens.ContactInformation;
import com.vmware.connectionapp.screens.Dashboard;
import com.vmware.connectionapp.screens.Login;
import com.vmware.connectionapp.screens.ManageMyTasksScreen;
import com.vmware.connectionapp.screens.MyGuides;
import com.vmware.connectionapp.screens.MyNews;
import com.vmware.connectionapp.screens.MyTasksScreen;
import com.vmware.connectionapp.screens.SearchScreen;
import com.vmware.connectionapp.screens.ViewAppsForMeScreen;
import com.vmware.connectionapp.screens.HamburgerMenu;
import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.setup.TestSuiteTearDown;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtentManager;
import com.vmware.connectionapp.utils.ExtentTestCase;
import com.vmware.connectionapp.utils.ScreenshotUtility;

/**
 * This class contains methods for all the test cases of fulfiller view.
 * 
 * @author Mindstix
 *
 */
public class TestScript {
  private static final Logger LOGGER = LoggerFactory.getLogger(TestScript.class);
  private BaseScreen objCommon = new BaseScreen();
  private ScreenshotUtility screenshotUtilObj = new ScreenshotUtility();
  private Login login = new Login();
  private MyTasksScreen myTasksScreen = new MyTasksScreen();
  private ContactInformation contactInformation = new ContactInformation();
  private Dashboard dashbaord = new Dashboard();
  private HamburgerMenu hamburgerMenu = new HamburgerMenu();
  private ManageMyTasksScreen manageMyTasksScreen = new ManageMyTasksScreen();
  private MyNews myNews = new MyNews();
  private MyGuides myGuides = new MyGuides();
  private ViewAppsForMeScreen viewAppsForMeScreen = new ViewAppsForMeScreen();
  private SearchScreen searchScreen = new SearchScreen();

  /**
   * Setup method gets called first before executing any test.
   */
  @BeforeTest(groups = { "smoke", "regression" })
  public void setup() {
    LOGGER.info("Set up for Connection Mobile Application ");
    ExtentTestCase.setEXTENT(ExtentManager.getExtent());
  }

  /**
   * TO verify Walk-through screens.
   */
  @Test(groups = { "smoke", "regression" }, priority = 1, enabled = true)
  public void verifyWalkthroughScreen() {
    LOGGER.info("Executing 1st test case : Walk through screens");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("TC-1 : Walk-Through Screen", "Walk-Through Module"));
    login.verifyWalkThroghScreens();
  }
  
  
  /**
   * Method to grant application related permissions & test application login.
   */
  @Test(groups = { "smoke", "regression" }, priority = 2, enabled = true)
  public void appLoginTest() {
    LOGGER.info("Executing 1st test case : Login Screen test.");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("TC-1 : To Verify Application Login", "Test the application login module."));
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      boolean isLoginSuccess = login.loginTestAndroid(Constants.USER_NAME, Constants.USER_PASSWORD);
      Assert.assertTrue(isLoginSuccess, "Login Failed ");
    } else {
      boolean isLoginSuccess = login.loginTestIOS(Constants.USER_NAME, Constants.USER_PASSWORD);
      Assert.assertTrue(isLoginSuccess, "Login Failed ");
    }
  }

  /**
   * Method to Contact Information Screen: UI validation from User Detail Api.
   */
  @Test(groups = { "smoke", "regression" }, priority = 3, enabled = false)
  public void uiValidationContactInfoScreen() {
    LOGGER.info("Executing 2nd test case : My Task Screen : Ui validation");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest(
        "To Verify My Contact Information Screen : Ui validation",
        "Contact Information Screen Module"));
    ExtentTestCase.getTest().info("Executing 2nd test case : Contact Information Screen : "
        + "Ui validation with User Detail Api ");
    contactInformation.UiValidationContatctInfo();
    ExtentTestCase.getTest().info("Executed 2nd test case : Contact Information Screen : "
        + "Ui validation with User Detail Api ");
  }

  /**
   * Method to test my task screen: UI validation.
   */
  @Test(groups = { "smoke", "regression" }, priority = 4, enabled = true)
  public void uiValidationMyTasksScreen() {
    LOGGER.info("Executing 3rd test case : My Task Screen : Ui validation");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("To Verify My task Screen : Ui validation", "My Task Screen Module"));
    ExtentTestCase.getTest().info("Executing 3rd test case : My Task Screen : Ui validation");
      myTasksScreen.navigationValidation();
      myTasksScreen.uiValidationMyTask();
    ExtentTestCase.getTest().info("Executed 3rd test case : My Task Screen : Ui validation");
  }

  /**
   * Method to test dashboard screen: UI validation.
   */
  @Test(groups = { "smoke", "regression" }, priority = 5, enabled = true)
  public void uiValidationForDashboardScreen() throws MalformedURLException, InterruptedException {
    LOGGER.info("Executing 4th test case : To Verify My Dashboard Screen : Ui validation");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("To Verify My dashboard Screen : Ui validation", "My dashboard Screen Module"));
    ExtentTestCase.getTest().info("Executing 4th test case : My dashboard Screen : Ui validation");
    TestSuiteSetup.restartApplication();
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      dashbaord.uiValidationDashboardScreenForAndroid();
    } else {
      dashbaord.uiValidationDashboardScreen();
    }
    ExtentTestCase.getTest().info("Executed 4th test case : My dashboard Screen : Ui validation");
  }

  /**
   * Method to test Hamburger Menu : Ui Validation and Navigation.
   */
  @Test(groups = { "smoke", "regression" }, priority = 6, enabled = true)
  public void uiValidationForHamburgerMenu() throws InterruptedException {
    LOGGER.info("Executing 5th test case : To Verify My Hamburher Menu  : Ui validation");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("To Verify My Hamburger Menu : Ui validation", "My Hamburger Menu Module"));
    ExtentTestCase.getTest().info("Executing 5th test case : My Hamburger Menu : Ui validation");
    TestSuiteSetup.restartApplication();
    hamburgerMenu.uiValidationForHamburger();
    hamburgerMenu.verifyNavigation();
    ExtentTestCase.getTest().info("Executed 5th test case : My Hamburger Menu : Ui validation");
  }

  /**
   * Method to test ui validation of My tasks screen : Onboarding and Training
   * screen
   * 
   * @throws InterruptedException
   */
  @Test(groups = { "smoke", "regression" }, priority = 7, enabled = false)
  public void uiValidationForMyTasksScreen() throws InterruptedException {
    LOGGER.info("Executing 7th test case : To Verify My Onboarding Screen  : Ui validation");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest(
        "To Verify My tasks screen : Onboarding and Training screen : Ui validation",
        "My Onboarding Screen Module"));
    ExtentTestCase.getTest().info(
        "Executing 6th test case : My Tasks Screen : Onboarding and Training screen : Ui validation");
      TestSuiteSetup.restartApplication();
    manageMyTasksScreen.verifyOnboardingScreen();
    ExtentTestCase.getTest().info(
        "Executed 6th test case : My Tasks Screen : Onboarding and Training screen : Ui validation");
  }

  /**
   * Method to test ui validation of Audio Conf Screen : My Tasks Screen.
   */
  @Test(groups = { "smoke", "regression" }, priority = 8, enabled = false)
  public void uiValidationForAudioSconSetupScreen()
      throws MalformedURLException, InterruptedException {
    LOGGER
        .info("Executing 7th test case : To Verify Audio Conference Setup Screen  : Ui validation");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest(
        "To Verify Audio Conference Setup Screen : Ui validation",
        "My Audio Conference Setup Screen Module"));
    ExtentTestCase.getTest()
        .info("Executing 7th test case : Audio Conference Setup Screen : Ui validation");
    TestSuiteSetup.restartApplication();
    manageMyTasksScreen.uiForAudioConfScreen();
    ExtentTestCase.getTest()
        .info("Executed 7th test case : Audio Conference Setup Screen : Ui validation");
  }

  /**
   * Method to test ui validation of Update Screen and news screen.
   */
  @Test(groups = { "smoke", "regression" }, priority = 9, enabled = false)
  public void uiValidationForUpdateScreen() throws MalformedURLException, InterruptedException {
    LOGGER.info("Executing 8th test case : To Verify Update Screen  : Ui validation");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("To Verify Update Screen : Ui validation", "My Update Screen Module"));
    ExtentTestCase.getTest().info("Executing 8th test case : Update Screen : Ui validation");
    TestSuiteSetup.restartApplication();
    myNews.verifyUpdateScreenValidation();
    ExtentTestCase.getTest().info("Executed 8th test case : Update Screen : Ui validation");
  }

  /**
   * Method to test ui validation and navigation of MyGuides Screen.
   */
  @Test(groups = { "smoke", "regression" }, priority = 10, enabled = false)
  public void uiValidationFateScreen() throws InterruptedException, MalformedURLException {
    LOGGER.info("Executing 9th test case : To Verify MyGuides Screen  : Ui validation");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("To Verify MyGuides Screen : Ui validation", "My MyGuides Screen Module"));
    ExtentTestCase.getTest().info("Executing 9th test case : MyGuides Screen : Ui validation");
      TestSuiteSetup.restartApplication();
    myGuides.verifyMyGuidesScreen();
    ExtentTestCase.getTest().info("Executed 9th test case : MyGuides Screen : Ui validation");
  }

  /**
   * Method to test ui validation and navigation of 'Apps for me' Screen. Verify
   * Apps count and vmware and workspace list.
   */
  @Test(groups = { "smoke", "regression" }, priority = 11, enabled = false)
  public void uiScreen() throws InterruptedException {
    LOGGER.info("Executing 10th test case : To Verify 'Apps for me' Screen  : Ui validation");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest(
        "To Verify 'Apps for me' Screen : Ui validation", "'Apps for me' Screen Module"));
    ExtentTestCase.getTest()
        .info("Executing 10th test case : 'Apps for me' Screen : Ui validation : "
            + "Verify Apps Count and vmware & workspace one list ");
      TestSuiteSetup.restartApplication();
    viewAppsForMeScreen.verifyAppsForMeScreen();
    viewAppsForMeScreen.verifyWorkspaceList();
    viewAppsForMeScreen.verifyVmwareList();
    viewAppsForMeScreen.verifyTotalApps();
    ExtentTestCase.getTest().info("Executed 10th test case : 'Apps for me' Screen : Ui validation");
  }

  /**
   * Method to test Updated titles validation from api for Update Screen.
   */
  @Test(groups = { "regression" }, priority = 12, enabled = false)
  public void verifyUpdatedTitlesValidationForUpdateScreen()
      throws MalformedURLException, InterruptedException {
    LOGGER.info(
        "Executing 11th test case : To Verify Update Screen  : Api Integration for Update Titles");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("To Verify Update Screen : Api Integration for Update Titles"));
    ExtentTestCase.getTest()
        .info("Executing 11th test case : Update Screen : Api Integration for Update Titles");
    myNews.verifyUpdateDetails();
    ExtentTestCase.getTest()
        .info("Executed 11th test case : Update Screen : Api Integration for Update Titles");
  }

  /**
   * Method to test News titles validation from Api for News screen.
   * 
   * @throws InterruptedException
   * Not covered in automation because it uses only images now
   */
  @Test(groups = { "regression" }, priority = 13, enabled = false)
  public void verifyNewsTitlesForNewsScreen() throws InterruptedException {
    LOGGER.info(
        "Executing 12th test case : To Verify News Screen  : Api Integration for News Titles");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("To Verify News Screen : Api Integration for News Titles"));
    ExtentTestCase.getTest()
        .info("Executing 12th test case : News Screen : Api Integration for News Titles");
    // Need to updated build with xpath for android : annoucement xpath 
    myNews.verifyNewsDetails();
    ExtentTestCase.getTest()
        .info("Executed 12th test case : News Screen : Api Integration for News Titles");
  }

  /**
   * Method to search functionality
   * 
   * @throws InterruptedException
   */
  @Test(groups = { "regression" }, priority = 14, enabled = false)
  public void verifySearchFuncationality() throws InterruptedException {
    LOGGER.info("Executing 13th test case : Seacrh Screen ");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("To Verify Search Screen : Api Integration for search functionality"));
    ExtentTestCase.getTest().info(
        "Executing 13th test case : Search Screen : Api Integration for search functionality");
      TestSuiteSetup.restartApplication();
    searchScreen.verifySearchFunctionality();
    ExtentTestCase.getTest()
        .info("Executed 13th test case : Seacrh Screen : Api Integration for search functionality");
  }

  /**
   * Method to test My Guides : Security Screen : Verify Heading and links.
   */
  @Test(groups = { "regression" }, priority = 15, enabled = false)
  public void verifySecurityScreen() throws InterruptedException {
    LOGGER.info("Executing 14th test case : To Verify My Guides : Security Screen. ");
    ExtentTestCase
        .setTest(ExtentTestCase.getExtent().createTest("To Verify My Guides : Security Screen."));
    ExtentTestCase.getTest()
        .info("Executing 14th test case : My Guides : Security Screen: Verify heading and links");
      TestSuiteSetup.restartApplication();
    myGuides.verifySecurityList();
    ExtentTestCase.getTest()
        .info("Executed 14th test case : My Guides : Security Screen: Verify heading and links");
  }

  /**
   * Method to test My Guides : Benefits Screen : Verify Heading and links.
   */
  @Test(groups = { "regression" }, priority = 16, enabled = false)
  public void verifyBenefitsScreen() throws InterruptedException {
    LOGGER.info("Executing 15th test case : To Verify My Guides : Benefits Screen. ");
    ExtentTestCase
        .setTest(ExtentTestCase.getExtent().createTest("To Verify My Guides : Benefits Screen."));
    ExtentTestCase.getTest()
        .info("Executing 15th test case : My Guides : Benefits Screen: Verify heading and links");
      TestSuiteSetup.restartApplication();
    myGuides.verifyBenefitsList();
    ExtentTestCase.getTest()
        .info("Executed 15th test case : My Guides : Benefits Screen: Verify heading and links");
  }

  /**
   * Method to test My Guides : Finance Screen : Verify Heading and links.
   */
  @Test(groups = { "regression" }, priority = 17, enabled = false)
  public void verifyFinanceScreen() throws InterruptedException {
    LOGGER.info("Executing 16th test case : To Verify My Guides : Finance Screen. ");
    ExtentTestCase
        .setTest(ExtentTestCase.getExtent().createTest("To Verify My Guides : Finance Screen."));
    ExtentTestCase.getTest()
        .info("Executing 16th test case : My Guides : Finance Screen: Verify heading and links");
      TestSuiteSetup.restartApplication();
    myGuides.verifyFinanceList();
    ExtentTestCase.getTest()
        .info("Executed 16th test case : My Guides : Finance Screen: Verify heading and links");
  }

  /**
   * Method to test Dashboard : Verify the unfinished task count displays
   * correctly on Manage my Tasks screen
   */
  @Test(groups = { "regression" }, priority = 18, enabled = false)
  public void verifyUnfinishedTaskCount() throws InterruptedException {
    LOGGER
        .info("Executing 17th test case : To Verify  Dashboard : Verify the unfinished task count");
    ExtentTestCase.setTest(ExtentTestCase.getExtent()
        .createTest("To Verify Dashboard : Verify the unfinished task count"));
    ExtentTestCase.getTest()
        .info("Executing 17th test case : Dashboard : Verify the unfinished task count");
    TestSuiteSetup.restartApplication();
    dashbaord.verifytaskCount();
    ExtentTestCase.getTest()
        .info("Executed 17th test case : Dashboard : Verify the unfinished task count");
  }

  /**
   * Executes after every test.
   * 
   * @param result
   * 
   * @throws IOException
   * @throws InterruptedException
   */
  @AfterMethod(groups = { "smoke", "regression" })
  public void getResult(ITestResult result) throws IOException, InterruptedException {
    LOGGER.info("In after method.");
    String screenShotDestination = null;
    if (result.getStatus() == ITestResult.FAILURE) {
      LOGGER.info("Test Failed.");
      ExtentTestCase.getTest().fail(Status.FAIL + ", Test Case Failed is " + result.getName());
      ExtentTestCase.getTest().fail(Status.FAIL + ", Test Case Failed is " + result.getThrowable());
      screenShotDestination = objCommon.captureScreenShot();
      screenshotUtilObj.onTestFailure(result);
      ExtentTestCase.getTest().log(Status.INFO, "Result Screenshot : "
          + ExtentTestCase.getTest().addScreenCaptureFromPath(screenShotDestination));
      try {
        ExtentTestCase.getTest().log(Status.FAIL,
            "Test Case Failed. Please find screenshot below."
                + " Right click and download the image to open." + "&lt;br&gt;&lt;br&gt;"
                + ExtentTestCase.getTest().addScreenCaptureFromPath(screenShotDestination));
      } catch (IOException e) {
        LOGGER.error("Error while performing actions on file.", e);
      }
      TestSuiteSetup.restartApplication();
      LOGGER.info("Screenshot Displayed in Extent Report");
    } else if (result.getStatus() == ITestResult.SKIP) {
      LOGGER.info("Test skipped.");
      ExtentTestCase.getTest().skip(Status.SKIP + ", '" + result.getName() + "' Test Case Skipped");
    } else if (result.getStatus() == ITestResult.SUCCESS) {
      LOGGER.info("Test Passed.");
      ExtentTestCase.getTest().pass(Status.PASS + ", '" + result.getName() + "' Test Case Passed.");
    }
    ExtentTestCase.getExtent().flush();
  }

  /**
   * Method to release resources.
   */
  @AfterTest(groups = { "smoke", "regression" })
  public void tear() {
    TestSuiteTearDown.teardown();
    LOGGER.info("After test executed");
  }
}
