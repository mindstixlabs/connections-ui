/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapp.api;

import static com.jayway.restassured.RestAssured.given;
import java.io.File;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import com.vmware.connectionapp.setup.ApiSetUp;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtendedUri;
import com.vmware.connectionapp.utils.ExtentTestCase;
import com.vmware.connectionapp.utils.HeadersParamsConstants;
import com.vmware.connectionapp.utils.RestAssuredUtility;

/**
 * This class contains methods to login with valid credentials and get get auth
 * code. This auth code can be used to generate access token and refresh token.
 * 
 * @author Mindstix
 *
 */
public class AccessAndRefreshToken {

  private static final Logger LOGGER = LoggerFactory.getLogger(AccessAndRefreshToken.class);
  private static WebDriver webDriver;
  private static String authCode;
  private static String refreshToken;
  private static String accessToken;
  private static String loginUrl;

  /**
   * Method to get access token.
   * 
   * @return access token
   */
  public static String getAccessToken() {
    return accessToken;
  }

  private static void setAccessToken(final String accessToken) {
    AccessAndRefreshToken.accessToken = accessToken;
  }

  /**
   * Method to setup the Base URI and Path then perform login with valid
   * credentials and generate the access and refresh token.
   */
  public static void apiSetup() {
    String driverPath = null;
    LOGGER.info("Machine OS : {} ", System.getProperty("os.name"));
    if (System.getProperty("os.name").contains("Mac")) {
      driverPath = new File("").getAbsolutePath() + File.separator + "src" + File.separator + "test" + File.separator
          + "resources" + File.separator + File.separator + "webdrivers" + File.separator + "mac_chrome_driver"
          + File.separator + "chromedriver";
    } else {
      driverPath = "Path to windows chrome driver ";
    }
    loginUrl = ApiSetUp.apiEnvProps.getProperty("baseURI_Auth") + ApiSetUp.apiEnvProps.getProperty("basePath_Auth")
        + ExtendedUri.EXTENDED_LOGIN_URI;
    System.setProperty("webdriver.chrome.driver", driverPath);
    LOGGER.info("Login URL : {} ", loginUrl);
    ChromeOptions headlessOption = new ChromeOptions();
    headlessOption.addArguments("--headless");
    webDriver = new ChromeDriver(headlessOption);
    webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    webDriver.get(loginUrl);
    RestAssuredUtility.setBaseUri(ApiSetUp.apiEnvProps.getProperty("baseURI_Auth"));
    RestAssuredUtility.setBasePath(ApiSetUp.apiEnvProps.getProperty("basePath_Auth"));
    loginWithValidCredentials();
    generateAccessTokenAndRefreshToken();
    if (webDriver != null) {
      webDriver.close();
    }
  }

  /**
   * Method to log in to the application.
   */
  private static void loginWithValidCredentials() {
    webDriver.findElement(By.id("username")).clear();
    webDriver.findElement(By.id("username")).sendKeys(RestAssuredUtility.decode(Constants.USER_NAME));
    LOGGER.info("Entered username");
    webDriver.findElement(By.id("password")).clear();
    webDriver.findElement(By.id("password")).sendKeys(RestAssuredUtility.decode(Constants.USER_PASSWORD));
    LOGGER.info("Entered password.");
    webDriver.findElement(By.id("signIn")).click();
    LOGGER.info("Clicked on sign in button");
    String currentUrl = webDriver.getCurrentUrl();
    LOGGER.info("Current URL : {}", currentUrl);
    if ((currentUrl != null && currentUrl.contains("redirect_uri"))) {
      WebDriverWait wait = new WebDriverWait(webDriver, 40);
      WebElement grantButton = webDriver.findElement(By.xpath("//*[@id='authorize']"));
      wait.until(ExpectedConditions.elementToBeClickable(grantButton));
      grantButton.click();
    }
    authCode = getAuthCode();
    LOGGER.info("Auth Code : {} ", authCode);
  }

  /**
   * Method to retrieve authentication code from the baseUrl.
   * 
   */
  private static String getAuthCode() {
    String url = webDriver.getCurrentUrl();
    LOGGER.info("URL: {}", url);
    String authCode = null;
    if (url.contains("code")) {
      authCode = url.substring(url.lastIndexOf("=") + 1);
      LOGGER.info("Äuthetication Code: {} ", authCode);
    }
    return authCode;
  }

  /**
   * Method to generate access token and refresh token.
   * 
   */
  public static void generateAccessTokenAndRefreshToken() {
    String accessTokenUrl = ExtendedUri.EXTENDED_ACCESS_TOKEN_URI_1;
    accessTokenUrl = accessTokenUrl + authCode + ExtendedUri.EXTENDED_ACCESS_TOKEN_URI_2;
    LOGGER.info("acessTokenURL:{}", accessTokenUrl);
    Response res;
    res = given().log().all().header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION)
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).when().post(accessTokenUrl).then()
        .statusCode(Constants.STATUSCODE_200).extract().response();
    refreshToken = res.getBody().jsonPath().getString("refresh_token");
    LOGGER.info("Refresh Token captured successfully : {} ", refreshToken);
    AccessAndRefreshToken.setAccessToken(res.getBody().jsonPath().getString("access_token"));
    LOGGER.info("Access Token captured successfully : {} ", accessToken);
  }

  /**
   * Method to reset base URI and base path.
   */
  public static void resetBaseUriAndBasePath() {
    ExtentTestCase.getTest().log(Status.INFO, "Resetting Base URI and Base Path");
    LOGGER.info("Resetting the Base URI and Base Path.");
    RestAssuredUtility.resetBaseUri();
    RestAssuredUtility.resetBasePath();
  }
}