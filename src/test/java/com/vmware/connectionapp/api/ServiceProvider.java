/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapp.api;

import static com.jayway.restassured.RestAssured.given;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.response.Response;
import com.vmware.connectionapp.setup.ApiSetUp;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtendedUri;
import com.vmware.connectionapp.utils.HeadersParamsConstants;
import com.vmware.connectionapp.utils.RestAssuredUtility;

/**
 * This class contains methods to call APIs required to for test cases.
 * 
 * @author Mindstix
 *
 */
public class ServiceProvider {
  private static final Logger LOGGER = LoggerFactory.getLogger(ServiceProvider.class);
  private Response response;

  /**
   * Method to set base URI and set base path.
   */
  public void setBaseUriAndSetBasePath() {
    RestAssuredUtility.setBaseUri(ApiSetUp.apiEnvProps.getProperty("baseURL"));
    LOGGER.info("Base URI :{} ", ApiSetUp.apiEnvProps.getProperty("baseURL"));
    RestAssuredUtility.setBasePath(ApiSetUp.apiEnvProps.getProperty("basePath"));
    LOGGER.info("Base Path :{} ", ApiSetUp.apiEnvProps.getProperty("basePath"));
  }

  /**
   * Method to get Updated details response.
   */
  public Response getUpdateDetails() {
    setBaseUriAndSetBasePath();
    response = given().log().all()
        .header(Constants.AUTHORIZATION,
            HeadersParamsConstants.AUTHORIZATION_ALEART + AccessAndRefreshToken.getAccessToken())
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.ACCEPT).when()
        .get(ExtendedUri.EXTENDED_GET_GLOBAL_COMM_URL).then().statusCode(Constants.STATUSCODE_200)
        .extract().response();
    LOGGER.info("Response captured Successfully");
//    LOGGER.info("REPSONSE : {} ", response.body().prettyPrint());
    return response;
  }
  
  /**
   * Method to get UserInfo response.
   */
  public Response getUserInfo() {
    setBaseUriAndSetBasePath();
    response = given().log().all()
        .header(Constants.AUTHORIZATION,
            HeadersParamsConstants.AUTHORIZATION_ALEART + AccessAndRefreshToken.getAccessToken())
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.ACCEPT).when()
        .get(ExtendedUri.EXTENDED_GET_USER_DETAIL_URL).then().statusCode(Constants.STATUSCODE_200)
        .extract().response();
    LOGGER.info("Response captured Successfully");
    LOGGER.info("REPSONSE : {} ", response.body().prettyPrint());
    return response;
  }
  
  /**
   * Method to get UserInfo response.
   */
  public Response getSearchInfo() {
    setBaseUriAndSetBasePath();
    response = given().log().all()
        .header(Constants.AUTHORIZATION,
            HeadersParamsConstants.AUTHORIZATION_ALEART + AccessAndRefreshToken.getAccessToken())
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.ACCEPT).when()
        .get(ExtendedUri.EXTENDED_SEARCH_DETAILS_URL).then().statusCode(Constants.STATUSCODE_200)
        .extract().response();
    LOGGER.info("Response captured Successfully");
 //   LOGGER.info("REPSONSE : {} ", response.body().prettyPrint());
    return response;
  }
  
  /**
   * Method to get UserInfo response.
   */
  public Response getAnnouncementInfo() {
    setBaseUriAndSetBasePath();
    response = given().log().all()
        .header(Constants.AUTHORIZATION,
            HeadersParamsConstants.AUTHORIZATION_ALEART + AccessAndRefreshToken.getAccessToken())
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.ACCEPT).when()
        .get(ExtendedUri.EXTENDED_ANNOUNCEMENT_URL).then().statusCode(Constants.STATUSCODE_200)
        .extract().response();
    LOGGER.info("Response captured Successfully");
    LOGGER.info("REPSONSE : {} ", response.body().prettyPrint());
    return response;
  }
}
