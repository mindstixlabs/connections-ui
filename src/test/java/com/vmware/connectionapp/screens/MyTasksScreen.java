/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.SkipException;

import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtentTestCase;

import io.appium.java_client.MobileElement;

/**
 * Contains a method of My Tasks Screen.
 * 
 * @author Mindstix
 */

public class MyTasksScreen extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(MyTasksScreen.class);

  /**
   * My tasks screen : Ui validation.
   */
  public void uiValidationMyTask() {
    getElement("uncheckbox_xpath").click();
    getElement("checkbox_xpath").click();
    List<String> taskList = new ArrayList<String>();
    isElementDisplayed("my_task_header_acc_id", "My task");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
    taskList = getTabList("task_title_xpath", "My Tasks", Constants.size);
    } else {
     taskList = getListItems("task_title_xpath", "My Tasks");
    }
    for (int i = 0; i < taskList.size(); i++) {
      String taskTitle= taskList.get(i).toString();
      String taskTitleExpected=Constants.MY_TASKS_LIST[i].toString();
      LOGGER.info("Actual : {} Expected : {} ", taskList.get(i).toString(),
          Constants.MY_TASKS_LIST[i].toString());
      Assert.assertEquals(taskTitle, taskTitleExpected);     
    }
    getElement("task_done_button_xpath").click();
    Assert.assertTrue(isElementPresent("search_acc_id"), "Search should be displayed");
    ExtentTestCase.getTest().info("User is on dashbaord");
  }

  /**
   * My task Screen : Navigation Validation.
   */
  public void navigationValidation() {
    waitUntilVisiblityWithTime("chevron_icon_xpath");
    List<MobileElement> chevronIconForTaskList = getElements("chevron_icon_xpath");
    for (int i = 0; i < chevronIconForTaskList.size(); i++) {
      if (i==5) {
        LOGGER.info("Request RSA Token Occured");
        break;
      }
      chevronIconForTaskList.get(i).click();
      getElement("backbutton_acc_id").click();
      LOGGER.info("Back to My Tasks Screen");
    }
  }
}
