/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtentTestCase;

/**
 * Methods for HamburgerMenu screen.
 * 
 * @author Mindstix
 *
 */

public class HamburgerMenu extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(HamburgerMenu.class);

  /**
   * Ui Validation of Hamburger Menu
   */
  public void uiValidationForHamburger() throws InterruptedException {
    doneButtondisplayed();
    isProgressBarDisplayed();
    waitUntilVisiblityWithTime("manage_my_task_xpath");
    clickElement("hamburger_acc_id", "Hamburger Icon");
  //  verifyUsernameAndDesignation();
    testElementAssert("contact_info_xpath", "My Contact Information");
    ExtentTestCase.getTest().info("My Contact Information is displayed ");
    testElementAssert("my_team_xpath", "My Team");
    ExtentTestCase.getTest().info("'My Team' is displayed ");
    testElementAssert("vmware_youtube_xpath", "VMware on Youtube");
    ExtentTestCase.getTest().info("'VMware on Youtube' is displayed ");
    testElementAssert("give_feedback_xpath", "Give Feedback");
    ExtentTestCase.getTest().info("'Give Feedback' is displayed ");
  }

  /**
   * Click on contact Information.
   */
  public void clickOnContactInformation() {
    waitUntilVisiblityWithTime("contact_info_xpath");
    getElement("contact_info_xpath").click();
    isProgressBarDisplayed();
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on My Team and get list of team member and team member designation.
   */
  public void clickOnMyTeam() {
    waitUntilVisiblityWithTime("my_team_xpath");
    getElement("my_team_xpath").click();
    isElementDisplayed("my_team_header_xpath", "'My Team' Header");
    waitUntilVisiblityWithTime("team_member_xpath");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      getTabList("team_member_xpath", "Team Member", Constants.One_time_scroll);
    } else {
      getListItems("team_member_xpath", "Team Member");

    }
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on Vmware on youtube.
   */
  public void clickOnVmwareOnYoutube() {
    getElement("vmware_youtube_xpath").click();
  }

  /**
   * Click on Give feedback and send feedback.
   */
  public void clickOnGiveFeedback() {
    clickElement("give_feedback_xpath", "feedback icon");
    testElementAssert("how_your_experience_xpath", Constants.HAMBUREGR_EXP_TEXT);
    clickElement("star_xpath", "'star icon'");
    ExtentTestCase.getTest().info("Sending text on feedback texbox");
    getElement("feedback_textbox_xpath").sendKeys("Automation Testing");
    hideKeyboard();
    clickElement("feedback_done_button_xpath", "Feedback Done Button");
    ExtentTestCase.getTest().info(getElementText("successfully_message_xpath"));
    clickElement("dismiss_button_id", "'Feedback pop up ok button'");
  }

  /**
   * Method to check navigation and verify My team,Feedback Screen elements.
   */
  public void verifyNavigation() {
    clickOnContactInformation();
    isProgressBarDisplayed();
    waitUntilVisiblityWithTime("hamburger_acc_id");
    getElement("hamburger_acc_id").click();
    LOGGER.info("Clicked on hamburger menu icon");
    clickOnMyTeam();
    isProgressBarDisplayed();
    waitUntilVisiblityWithTime("hamburger_acc_id");
    getElement("hamburger_acc_id").click();
    clickOnGiveFeedback();
    waitUntilVisiblityWithTime("hamburger_acc_id");
  }

  /**
   * Hamburger Menu : Verify User name and its designation.
   */
  public void verifyUsernameAndDesignation() {
    testElementAssert("hamburger_username_acc_id", Constants.HAMBURGER_USERNAME);
    ExtentTestCase.getTest().info("User name is displayed ");
    testElementAssert("hamburger_username_designation_acc_id", Constants.HAMBURGER_DESIGNATION);
    ExtentTestCase.getTest().info("User designation is displayed");
  }
}
