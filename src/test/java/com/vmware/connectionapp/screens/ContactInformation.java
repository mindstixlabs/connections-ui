/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import java.util.ArrayList;
import java.util.List;
import org.testng.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.vmware.connectionapp.api.ServiceProvider;
import com.vmware.connectionapp.utils.ExtentTestCase;
import com.vmware.connectionapp.utils.RestAssuredUtility;

import io.appium.java_client.MobileElement;

/**
 * Contains User Info
 * 
 * @author Mindstix
 *
 */
public class ContactInformation extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(ContactInformation.class);
  private ServiceProvider serviceProvider = new ServiceProvider();
  private Response response;

  /**
   * Method to test Contact Information.
   */
  public void UiValidationContatctInfo() {
    isElementDisplayed("contact_info_header_xpath", "My Contact Information");
    List<MobileElement> keyLabels = getElements("contact_key_label_xpath");
    List<MobileElement> keyValues = getElements("contact_key_value_xpath");
    List<String> userDetailsList = getUserDetail();
    for (int i = 0; i < keyLabels.size(); i++) {
      String key = keyLabels.get(i).getText();
      String keyValue = keyValues.get(i).getText();
      String keyValueFromApi = userDetailsList.get(i).toString();
      LOGGER.info("Key : {} , Value : {} ", key, keyValue);
      ExtentTestCase.getTest().info("Key : " + key + ", Value : " + keyValue);
      ExtentTestCase.getTest().info("User Details from Api Response : " + keyValueFromApi);
      Assert.assertEquals(keyValue, keyValueFromApi);
      ExtentTestCase.getTest()
          .info(key + " =: Actual : " + keyValueFromApi + " Expected : " + keyValue);
    }
    MobileElement nextButton = getElement("next_button_xpath");
    LOGGER.info("Element found");
    nextButton.click();
  }

  /**
   * Method to get Response User detail Info and extract username , email id
   * mobile number and office address.
   */
  public List<String> getUserDetail() {
    response = serviceProvider.getUserInfo();
    JsonPath jp = RestAssuredUtility.getJsonPath(response);
    LOGGER.info("Extracting JSON");
    String userName = jp.getString("response.success.data.cn");
    String phoneNumber = jp.getString("response.success.data.mobileNumber");
    String emailId = jp.getString("response.success.data.principal");
    String street = jp.getString("response.success.data.street");
    String city = jp.getString("response.success.data.city");
    String state = jp.getString("response.success.data.state");
    String country = jp.getString("response.success.data.country");
    String officeAddress = street + " " + city + " " + state + " " + country;
    LOGGER.info(userName);
    LOGGER.info(phoneNumber);
    LOGGER.info(emailId);
    LOGGER.info(officeAddress);
    List<String> userInfoList = new ArrayList<String>();
    userInfoList.add(userName);
    userInfoList.add(phoneNumber);
    userInfoList.add(emailId);
    userInfoList.add(officeAddress);
    return userInfoList;
  }
}
