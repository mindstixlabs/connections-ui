/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aventstack.extentreports.Status;
import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.utils.ExtentTestCase;

/**
 * This class contains methods to login with valid credentials and also contains
 * test methods for login screen.
 * 
 * @author Mindstix
 * 
 */
public class Login extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(Login.class);

  /**
   * Login test method.
   * 
   * @param username
   *          user name for login.
   * @param password
   *          password for login.
   * @return true if logged in successfully.
   */
  public boolean loginTestAndroid(String username, String password) {
    getElement("login_button_acc_id").click();
    getElement("web_usernmae_field_id").sendKeys(decode(username));
    hideKeyboard();
    LOGGER.info("Entered Username.");
    ExtentTestCase.getTest().log(Status.INFO, "Action: Entered Username.");
    getElement("web_password_id").sendKeys(decode(password));
    hideKeyboard();
    LOGGER.info("Entered Password.");
    ExtentTestCase.getTest().log(Status.INFO, "Action: Entered Password");
    getElement("web_signin_button_id").click();
    ExtentTestCase.getTest().log(Status.INFO, "Action: Clicked on sign in button.");
    ExtentTestCase.getTest().log(Status.INFO, "Logging into the application.");
    ExtentTestCase.getTest().log(Status.INFO, "Login: Success");  
    waitUntilVisiblityWithTime("contact_info_header_xpath");
    ExtentTestCase.getTest().log(Status.INFO, "contact information screen successfully");
    return isElementPresent("contact_info_header_xpath");
  }

  /**
   * Login test method.
   * 
   * @param username
   *          user name for login.
   * @param password
   *          password for login.
   * @return true if logged in successfully.
   */
  public boolean loginTestIOS(String username, String password) {
    getElement("login_button_acc_id").click();    
    getElement("web_username_field_xpath").sendKeys(decode(username));
    ExtentTestCase.getTest().log(Status.INFO, "Action: Entered Username.");
    LOGGER.info("Entered Username");
    hideKeyboard();
    getElement("web_password_field_xpath").sendKeys(decode(password));
    LOGGER.info("Entered Password");
    ExtentTestCase.getTest().log(Status.INFO, "Action: Entered password.");
    getElement("web_signin_button_acc_id").click();
    LOGGER.info("Clicked on sign in button");
    ExtentTestCase.getTest().log(Status.INFO, "Action: Clicked on sign in.");
    clickOnGrantButton();
    LOGGER.info("contact information screen successfully");
    ExtentTestCase.getTest().log(Status.INFO, "Login: Success.");
    ExtentTestCase.getTest().log(Status.INFO, "contact information screen successfully");
    waitUntilVisiblityWithTime("contact_info_header_acc_id");
    return isElementPresent("contact_info_header_acc_id");
  }

  
  /**
   * click on Grant Button.
   */
  public void clickOnGrantButton() {   
    if (isElementDisplayed("grant_button_acc_id", "Grant Button")) {
      clickElement("grant_button_acc_id", "Grant Button");
    }
  }
  
  /**
   * Verify Walkthrough screens.
   */
  public void verifyWalkThroghScreens() {
    ExtentTestCase.getTest().log(Status.INFO, "Device OS: Android");
    ExtentTestCase.getTest().log(Status.INFO, "Started: Executing Permissions Test.");
    ExtentTestCase.getTest().log(Status.INFO, "Allowing application permissions.");
    clickElement("allow_button_id", "Allow button"); 
//    for (int i=0; i<2;i++) {
//      swipeTaskLeft();
//    } 
//    waitUntilVisiblityWithTime("close_button_xpath");
//    clickElement("close_button_xpath", "Close Button");
    clickElement("next_button_Walkthrough_xpath", "Next Button");
    waitUntilVisiblityWithTime("skip_button_xpath");
    clickElement("skip_button_xpath", "Skip Button");
    ExtentTestCase.getTest().info("User is on login screen");
  }
}
