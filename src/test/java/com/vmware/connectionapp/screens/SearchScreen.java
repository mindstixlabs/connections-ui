/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.vmware.connectionapp.api.ServiceProvider;
import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtentTestCase;
import com.vmware.connectionapp.utils.RestAssuredUtility;

/**
 * Contains a method to test search functionality.
 * 
 * @author Mindstix
 */

public class SearchScreen extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(SearchScreen.class);
  private ServiceProvider serviceProvider = new ServiceProvider();
  private Response response;
  List<String> searchTiles = new ArrayList<String>();

  /**
   * Verify Search Functionality: Calling search Api and extract search titles
   * from api and validate with ui .
   */
  public void verifySearchFunctionality() throws InterruptedException {
    doneButtondisplayed();
    response = serviceProvider.getSearchInfo();
    JsonPath json = RestAssuredUtility.getJsonPath(response);
    ExtentTestCase.getTest().info("User is on dashboard and clicking on Search icon");
    waitUntilVisiblityWithTime("manage_my_task_xpath");
    clickElement("search_acc_id", "search icon");
    waitUntilVisiblityWithTime("search_bar_id");
    getElement("search_bar_id").sendKeys("take");
    hideKeyboard();
    waitUntilVisiblityWithTime("search_result_xpath");
    searchResults(json);
    clickElement("search_cancel_icon_xpath", "Search Cancel Button");
    waitUntilVisiblityWithTime("search_bar_id");
    isElementDisplayed("minimum_char_search_xpath", "'Enter minimum 3 characters to search'");
    getElement("backbutton_acc_id").click();
    Assert.assertTrue(isElementPresent("search_acc_id"), "Search Icon should be displayed");
  }

  /**
   * Json path traversing for Search Results.
   */
  public void searchResults(JsonPath jsonForSearchItems) {
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      searchTiles = getTabList("search_result_xpath", "Search Screen", Constants.One_time_scroll);
    } else {
      searchTiles = getListItems("search_result_xpath", "Search Screen Item list");
    }
    for (int i = 0; i < searchTiles.size(); i++) {
      String jsonPathIndex = "GSP.RES.R[" + i + "].T";
      LOGGER.info("JSON PATH : {} ", jsonPathIndex);
      String searchTitleFromApi = jsonForSearchItems.getString(jsonPathIndex);
      String searchTitleFromUi = searchTiles.get(i).toString();
      LOGGER.info("Search Title from Api : {} ", searchTitleFromApi);
      LOGGER.info("Search Title from Ui : {} ", searchTitleFromUi);
      ExtentTestCase.getTest().info("Actual Search title : searchTitleFromApi"
          + "Expected Search title : " + searchTitleFromUi);
    }
  }
}
