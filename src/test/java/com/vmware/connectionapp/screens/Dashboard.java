/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import org.slf4j.LoggerFactory;
import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtentTestCase;
import io.appium.java_client.MobileElement;
import java.util.List;
import org.slf4j.Logger;

/**
 * Contains method of dashboard
 * 
 * @author Mindstix
 *
 */
public class Dashboard extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(Dashboard.class);

  /**
   * Ui Validation on dashboard screen : IOS.
   */
  public void uiValidationDashboardScreen() throws InterruptedException {
    doneButtondisplayed();
    String greetingMsg = getElementText("hello_label_xpath");
    ExtentTestCase.getTest().info("Greeting Message " + greetingMsg);
    List<MobileElement> dashboardItems = getElements("dashboard_xpath");
    isElementDisplayed("hamburger_acc_id", "Hamburger Icon");
    isElementDisplayed("search_acc_id", "Search Icon label");
    for (int i = 0; i < dashboardItems.size(); i++) {
      String dashboardItem = dashboardItems.get(i).getText();
      String dashboardItemFromUi = Constants.DASHBOARD_LINK_NAMES[i].toString();
      LOGGER.info("Actual label : {} Expected label : {} ", dashboardItem, dashboardItemFromUi);
      ExtentTestCase.getTest()
          .info("Actual label : " + dashboardItem + " Expected label : {} " + dashboardItemFromUi);
      ExtentTestCase.getTest().info(dashboardItem + " label is displayed");
    }
  }

  /**
   * Verify Ui Validation on dashboard screen : Android.
   */
  public void uiValidationDashboardScreenForAndroid() throws InterruptedException {
    doneButtondisplayed();
    String greetingMsg = getElementText("hello_label_xpath");
    LOGGER.info("Greetign Message : {} ", greetingMsg);
    ExtentTestCase.getTest().info("Greetign Message " + greetingMsg);
    testElementAssert("manage_my_task_xpath", "Manage My Tasks");
    LOGGER.info("'Manage My Tasks' label is displayed");
    ExtentTestCase.getTest().info("'Manage My Tasks' label is displayed");
    testElementAssert("views_news_xpath", "View Updates & News");
    LOGGER.info("'View Updates & News' label is displayed");
    ExtentTestCase.getTest().info("'View News & Alerts' label is displayed");
    customVerticalSwipe(0.80, 0.20);
    testElementAssert("search_guides_xpath", "Search Guides for Help");
    LOGGER.info("'Search Guides for Help' label is displayed");
    ExtentTestCase.getTest().info("'Search Guides for Help' label is displayed");
    testElementAssert("view_apps_xpath", "View Apps for Me");
    LOGGER.info("'View Apps for Me' label is displayed");
    ExtentTestCase.getTest().info("'View Apps for Me' label is displayed");
  }

  /**
   * Dashboard : Verify the unfinished task count.
   */
  public void verifytaskCount() throws InterruptedException {
    String count;
    doneButtondisplayed();
    waitUntilVisiblityWithTime("manage_my_task_xpath");
    count = getCountOfUnfinishedTask();
    ExtentTestCase.getTest().info("Count of Unfinished Tasks Before Swipping : " + count);
    getElement("manage_my_task_xpath").click();
    waitUntilVisiblityWithTime("task_header_xpath");
    swipeTaskLeft();
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      getElement("swipe_done_button_xpath").click();
      getElements("done_button_after_swipe_xpath").get(1).click();
    } else {
      clickDoneButton("DONE");
    }
    getElement("backbutton_acc_id").click();
    count = getCountOfUnfinishedTask();
    ExtentTestCase.getTest()
        .info("Count of Unfinished Tasks After Swipping :Should be decremented by one: " + count);
  }

  /**
   * Get Unfinished Count.
   */
  public String getCountOfUnfinishedTask() {
    String unfinishedTasktext = getElementText("unfinished_tasks_count_xpath");
    ExtentTestCase.getTest().info("Unfinished Tasks : " + unfinishedTasktext);
    LOGGER.info("Unfinished Tasks String : {} ", unfinishedTasktext);
    String count = unfinishedTasktext.substring(8, 11);
    LOGGER.info("Unfinished Tasks count : {} ", count);
    return count;
  }

  /**
   * Get Done Button Elements.
   */
  public void clickDoneButton(String name) {
    List<MobileElement> doneButtonList = TestSuiteSetup.driver.findElementsByName(name);
    LOGGER.info("Size of list : {} ", doneButtonList);
    for (int i = 0; i < doneButtonList.size(); i++) {
      doneButtonList.get(i).click();
      if (isElementDisplayed("task_pop_up_xpath", "Task pop up ")) {
        getElement("done_button_after_swipe_acc_id").click();
        break;
      }
    }
  }
}
