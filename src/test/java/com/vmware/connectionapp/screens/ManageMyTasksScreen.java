/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import java.net.MalformedURLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtentTestCase;


/**
 * Contains method of Onboarding and training screen.
 * 
 * @author Mindstix
 *
 */
public class ManageMyTasksScreen extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(ManageMyTasksScreen.class);

  /**
   * Click on manage my tasks and verify onboarding screen
   */
  public void verifyOnboardingScreen() throws InterruptedException {
    doneButtondisplayed();
    getElement("manage_my_task_xpath").click();
    testElementAssert("task_header_acc_id", "MY TASKS");
    ExtentTestCase.getTest().info("'My Tasks' heading is displayed");
    testElementAssert("onboarding_xpath", "Onboarding");
    ExtentTestCase.getTest().info("'Onboarding' label is displayed");
    testElementAssert("training_xpath", "Training");
    ExtentTestCase.getTest().info("'Training' label is displayed");
    uiForDeskPhoneSetupScreen();
    uiForPrinterSetupScreen();
    uiForBadgeReceivedScreen();
    uiForPSyncPasswordScreen();
    uiForOffice365Screen();
    uiForEmailSignatureScreen();
    uiForSkypeBusinessScreen();
    uiForWorkspaceScreen();
    uiForContactInfoScreen();
    uiForHrHomepageScreen();
    uiForMyGoalsScreen();
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on desk phone setup and ui and navigation validation.
   */
  public void uiForDeskPhoneSetupScreen() {
    getElement("desk_phone_setup_chevron_xpath").click();
    testElementAssert("desk_phone_setup_xpath", "DESK PHONE SETUP");
    ExtentTestCase.getTest().info("'DESK PHONE SETUP' heading is displayed");
    testElementAssert("log_into_extension_xpath", "Log into Extension");
    ExtentTestCase.getTest().info("'Log into Extension' label is displayed");
    testElementAssert("enter_five_digit_xpath", "Enter your five digit extension");
    ExtentTestCase.getTest().info("'Enter your five digit extension' label is displayed");
    testElementAssert("press_enter_button_xpath", "Press Enter button or hit # key");
    ExtentTestCase.getTest().info("'Press Enter button or hit # key' label is displayed");
    testElementAssert("key_password_xpath", "Key in password 1234");
    ExtentTestCase.getTest().info("'Key in password 1234' label is displayed");
    testElementAssert("press_enter_second_xpath", "Press Enter button or hit # key");
    ExtentTestCase.getTest().info("'Press Enter button or hit # key' label is displayed");
    testElementAssert("all_set_xpath", "All set!");
    ExtentTestCase.getTest().info("'All set!' label is displayed");
    customVerticalSwipe(0.80, 0.20);
    testElementAssert("logout_extension_xpath", "Log out of Extension");
    ExtentTestCase.getTest().info("'Log out of Extension' label is displayed");
    testElementAssert("press_home_button_xpath", "Press home(A) button");
    ExtentTestCase.getTest().info("'Press home(A) button' label is displayed");
    testElementAssert("scroll_down_logout_xpath", "Scroll down and select logout");
    ExtentTestCase.getTest().info("'Scroll down and select logout' label is displayed");
    swipeUp();
    testElementAssert("press_logout_xpath", "Press Logout or hit OK");
    ExtentTestCase.getTest().info("'Press Logout or hit OK' label is displayed");
    testElementAssert("press_logout_confirm_xpath", "Press Logout again to confirm");
    swipeUp();
    ExtentTestCase.getTest().info("'Press Logout again to confirm' label is displayed");
    testElementAssert("logout_complete_xpath", "Logout Complete!");
    ExtentTestCase.getTest().info("'Logout Complete!' label is displayed");
    getElement("backbutton_acc_id").click();

  }

  /**
   * Click on desk phone setup and ui and navigation validation.
   */
  public void uiForPrinterSetupScreen() {
    getElement("printer_setup_chevron_xpath").click();
    testElementAssert("printer_setup_heading_xpath", "Printer Setup");
    ExtentTestCase.getTest().info("'Printer Setup' heading is displayed");
    testElementAssert("printer_link_xpath", Constants.PRINTER_LINK);
    ExtentTestCase.getTest().info("'link'  is displayed");
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on desk phone setup and ui and navigation validation.
   */
  public void uiForAudioConfScreen() throws MalformedURLException, InterruptedException {
    doneButtondisplayed();
    getElement("manage_my_task_xpath").click();
    waitUntilVisiblityWithTime("audio_conference_setup_chevron_xpath");
    getElement("audio_conference_setup_chevron_xpath").click();
    testElementAssert("audio_conf_heading_xpath", "AUDIO CONFERENCING SETUP");
    ExtentTestCase.getTest().info("'AUDIO CONFERENCING SETUP' heading is displayed");
    stepsForSetup();
    swipeUp();
    testElementAssert("pgi_account_xpath", "REQUEST FOR PGi ACCOUNT");
    ExtentTestCase.getTest().info("'REQUEST FOR PGi ACCOUNT' button is displayed");
    testElementAssert("pgi_partner_portal_xpath", "VISIT PGi PARTNER PORTAL");
    ExtentTestCase.getTest().info("'VISIT PGi PARTNER PORTAL' button is displayed");   
    getElement("backbutton_acc_id").click();
    getElement("backbutton_acc_id").click();
  }

  /**
   * Audio Conference Setup Steps
   */
  public void stepsForSetup() {
    ExtentTestCase.getTest()
        .info("Setup your conference call account description : " + getElementText("audio_setup_xpath"));
    ExtentTestCase.getTest()
        .info("Steps to set up conference call : " + getElementText("setup_steps_xpath"));
  }

  /**
   * Click on desk phone setup and ui and navigation validation.
   */
  public void uiForBadgeReceivedScreen() {
    getElement("badge_received_chevron_xpath").click();
    testElementAssert("badge_heading_xpath", "REQUEST FOR ID");
    ExtentTestCase.getTest().info("'REQUEST FOR ID' heading is displayed");
    testElementAssert("email_help_desk_xpath", "EMAIL HELP DESK");
    ExtentTestCase.getTest().info("'EMAIL HELP DESK' heading is displayed");
    removeHamburgerSlider();
    getElement("backbutton_acc_id").click();

  }

  /**
   * Click on desk phone setup and ui and navigation validation.
   */
  public void uiForPSyncPasswordScreen() {
    getElement("p_sync_password_chevron_xpath").click();
    testElementAssert("p_sync_password_heading_xpath", "P-synch Password Change");
    ExtentTestCase.getTest().info("'P-synch Password Change' heading is displayed");
    testElementAssert("p_sync_link_xpath", "https://psynch.vmware.com");
    ExtentTestCase.getTest().info("' https://psynch.vmware.com' link is displayed");
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on request_rsa_chevron and ui and navigation validation.
   */
  public void uiForRequestRsaScreen() {
    swipeUp();
    getElement("request_rsa_chevron_xpath").click();
    removeHamburgerSlider();
  }

  /**
   * Click on offcie_365_chevron and ui and navigation validation.
   */
  public void uiForOffice365Screen() {
    getElement("offcie_365_chevron_xpath").click();
    testElementAssert("office_365_heading_xpath", "Office 365");
    ExtentTestCase.getTest().info("'Office 365' heading is displayed");
    testElementAssert("office_365_link_xpath", Constants.OFFICE_365_LINK);
    ExtentTestCase.getTest().info(Constants.OFFICE_365_LINK + " link is displayed");
    getElement("backbutton_acc_id").click();
    waitUntilVisiblityWithTime("offcie_365_chevron_xpath");
    customVerticalSwipe(0.70, 0.20);
  }

  /**
   * Click on email_signature_chevron icon and ui and navigation validation.
   */
  public void uiForEmailSignatureScreen() throws InterruptedException {
    getElement("email_signature_chevron_xpath").click();
    testElementAssert("email_signature_heading_xpath", "Email Signature & VMware Template");
    ExtentTestCase.getTest().info("'Email Signature & VMware Template' heading is displayed");
    testElementAssert("email_signature_link_xpath", Constants.EMAIL_SIGNATURE_LINK);
    ExtentTestCase.getTest().info(Constants.EMAIL_SIGNATURE_LINK + " link is displayed");
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on skype_business_chevron icon and ui and navigation validation.
   */
  public void uiForSkypeBusinessScreen() {
    getElement("skype_business_chevron_xpath").click();
    testElementAssert("skype_business_heading_xpath", "Skype for Business");
    ExtentTestCase.getTest().info("'Skype for Business' heading is displayed");
    testElementAssert("skype_business_link_xpath", Constants.SKYPE_BUISNESS_LINK);
    ExtentTestCase.getTest().info(Constants.SKYPE_BUISNESS_LINK + " link is displayed");
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on workspace_one_chevron icon and ui and navigation validation.
   */
  public void uiForWorkspaceScreen() {
    getElement("workspace_one_chevron_xpath").click();
    testElementAssert("workspace_heading_xpath", "Workspace ONE");
    ExtentTestCase.getTest().info("'Workspace ONE' heading is displayed");
    testElementAssert("workspace_one_link_xpath", Constants.WORKSPACE_ONE_LINK);
    ExtentTestCase.getTest().info(Constants.WORKSPACE_ONE_LINK + " link is displayed");
    testElementAssert("workspace_one_go_to_apps_xpath", "GO TO MY APPS");
    ExtentTestCase.getTest().info("'GO TO MY APPS' button is displayed");
//    clickElement("workspace_one_go_to_apps_xpath", "'GO TO MY APPS' link");
//    testElementAssert("apps_for_me_heading_xpath", "APPS FOR ME");
//    ExtentTestCase.getTest().info("'APPS FOR ME' heading is displayed");
//    getElements("backbutton_acc_id").get(3).click();
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on work_home_contact_chevron icon and ui and navigation validation.
   */
  public void uiForContactInfoScreen() {
    getElement("work_home_contact_chevron_xpath").click();
    testElementAssert("work_home_contact_heading_xpath", "Work/Home Contact Information");
    ExtentTestCase.getTest().info("'Work/Home Contact Information' heading is displayed");
    testElementAssert("work_home_contact_link_xpath", Constants.WORKDAY_LINK);
    ExtentTestCase.getTest().info(Constants.WORKDAY_LINK + " link is displayed");
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on hr_homepage_chevron icon and ui and navigation validation.
   */
  public void uiForHrHomepageScreen() {
    getElement("hr_homepage_chevron_xpath").click();
    testElementAssert("hr_homepage_heading_xpath", "HR Homepage on VMware Source");
    ExtentTestCase.getTest().info("'HR Homepage on VMware Source' heading is displayed");
    testElementAssert("work_home_contact_link_xpath", Constants.HR_GROUP_LINK);
    ExtentTestCase.getTest().info(Constants.HR_GROUP_LINK + " link is displayed");
    getElement("backbutton_acc_id").click();
  }

  /**
   * Click on my_goals_chevron icon and ui and navigation validation.
   */
  public void uiForMyGoalsScreen() {
    getElement("my_goals_chevron_xpath").click();
    testElementAssert("my_goals_heading_xpath", "My Goals");
    ExtentTestCase.getTest().info("' My Goals' heading is displayed");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
    getTabList("my_goals_text_xpath", "My Goals", Constants.One_time_scroll);
    } else {
      getListItems("my_goals_text_xpath", "My Goals");
    }
    testElementAssert("my_goals_click_here_xpath", "Click here to update your goals.");
    ExtentTestCase.getTest().info("'Click here to update your goals.' link is displayed");
    getElement("backbutton_acc_id").click();
  }

}
