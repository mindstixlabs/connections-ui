/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtentTestCase;

import io.appium.java_client.MobileElement;

/**
 * Contains method of My Guides Screen
 * 
 * @author Minstix
 *
 */
public class MyGuides extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(MyGuides.class);
  private List<String> titles = new ArrayList<String>();
  private List<String> subTitles = new ArrayList<String>();
  private List<MobileElement> titlesName = new ArrayList<MobileElement>();
  private int totalTitleNumber = 0;
  private int totalTitleNumberGuides = 0;

  /**
   * Verify My Guides Screen.
   */
  public void verifyMyGuidesScreen() throws InterruptedException {
    doneButtondisplayed();
    waitUntilVisiblityWithTime("search_guides_count_xpath");
    totalTitleNumberGuides = getGuidesNumber("search_guides_count_xpath", 20, 22);
    ExtentTestCase.getTest().info("User is on dasboard and navigating to my guides screen");
    getElement("search_guides_xpath").click();
    testElementAssert("my_guides_screen_heading_xpath", "MY GUIDES");
    ExtentTestCase.getTest().info("'MY GUIDES' heading is displayed");
    verifySetUpTab();
    verifyFinanceTab();
    verifyBenefitsTab();
    verifySecurityTab();
    Assert.assertEquals(totalTitleNumber, totalTitleNumberGuides);
    ExtentTestCase.getTest().info("Actual Guides Number : " + totalTitleNumber
        + " Expected Guides Number : " + totalTitleNumberGuides);
    getElement("backbutton_acc_id").click();
  }

  /**
   * Get Total No. of Guides.
   */
  public int getGuidesNumber(String xpath, int startString, int endString) {
    String guidesNumber = getElementText(xpath);
    ExtentTestCase.getTest().info("Guides String : " + guidesNumber);
    LOGGER.info("Guides String : {} ", guidesNumber);
    String count = guidesNumber.substring(startString, endString);
    LOGGER.info("Total Number of Guides :{} ", count);
    int totalTitleNumberGuides = Integer.parseInt(count);
    ExtentTestCase.getTest()
        .info("Guides String : " + guidesNumber + " Total Guides Count: " + totalTitleNumberGuides);
    return totalTitleNumberGuides;
  }

  /**
   * Click on finance tab and check finance list .
   */
  public void verifyFinanceTab() {
    getElements("finance_xpath").get(1).click();
    isElementDisplayed("finance_xpath", "Finance label");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("tab_list_xpath", "Finance", Constants.One_time_scroll);
      LOGGER.info("size of finance list :{} ", titles.size());
      totalTitleNumber = totalTitleNumber + titles.size();
    } else {
      titlesName = getTabList("tab_list_xpath", "Finance");
      LOGGER.info("size of finance list :{} ", titlesName.size());
      totalTitleNumber = totalTitleNumber + titlesName.size();
    }
  }

  /**
   * Click on benefits tab and check Benefits list .
   */
  public void verifyBenefitsTab() {
    getElements("benefits_xpath").get(2).click();
    isElementDisplayed("benefits_xpath", "Benefits label");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("tab_list_xpath", "Benefits", Constants.One_time_scroll);
      LOGGER.info("size of benefits list :{} ", titles.size());
      totalTitleNumber = totalTitleNumber + titles.size();
    } else {
      titlesName = getTabList("tab_list_xpath", "Benefits");
      LOGGER.info("size of benefits list :{} ", titlesName.size());
      totalTitleNumber = totalTitleNumber + titlesName.size();
    }
  }

  /**
   * Click on Security tab and check Security list .
   */
  public void verifySecurityTab() {
    getElements("security_xpath").get(3).click();
    isElementDisplayed("security_xpath", "Security label");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("tab_list_xpath", "Security", Constants.One_time_scroll);
      LOGGER.info("size of Security list :{} ", titles.size());
      totalTitleNumber = totalTitleNumber + titles.size();
    } else {
      titlesName = getTabList("tab_list_xpath", "Security");
      LOGGER.info("size of Security list :{} ", titlesName.size());
      totalTitleNumber = totalTitleNumber + titlesName.size();
    }
  }

  /**
   * Click on Setup tab and check Setup list .
   */
  public void verifySetUpTab() throws InterruptedException {
    getElement("setup_xpath").click();
    isElementDisplayed("setup_xpath", "Setup link");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("setup_list_xpath", "Setup", Constants.size);
      LOGGER.info("size of Setup list :{} ", titles.size());
      totalTitleNumber = totalTitleNumber + titles.size();
    } else {
      titlesName = getTabList("setup_list_xpath", "Setup");
      LOGGER.info("size of Setup list :{} ", titlesName.size());
      totalTitleNumber = totalTitleNumber + titlesName.size();
    }
  }

  /**
   * Verify Security list.
   */
  public void verifySecurityList() throws InterruptedException {
    navigateToMyGuideScreen("security_xpath", 3);
    ExtentTestCase.getTest().info(
        "Navigating 'P-synch Passwaor change' and P-synch Passwaor change Screen is displayed");
    pSyncPasswordScreen();
    ExtentTestCase.getTest()
        .info("Navigating on 'Request for id' and 'Request for id' Screen is displayed");
    requestForId();
    ExtentTestCase.getTest().info("Navigating on 'Guidelines' and Guidelines Screen is displayed");
    guidelineScreen();
    getElement("backbutton_acc_id").click();
    ExtentTestCase.getTest().info("Navigated to Dashboard Screen");
  }

  /**
   * My guides screen : Navigate to Security tab, benefits tab, finance and setup
   * tab.
   */
  public void navigateToMyGuideScreen(String xpath, int index) throws InterruptedException {
    doneButtondisplayed();
    ExtentTestCase.getTest().info("User is on dasboard and navigating to my guides screen");
    getElement("search_guides_xpath").click();
    getElements(xpath).get(index).click();
  }

  /**
   * click 'P-synch password' link and verify screen heading and link of 'P-synch
   * password' change screen.
   */
  public void pSyncPasswordScreen() {
    getElement("tab_list_xpath").click();
    testElementAssert("security_p_sync_pws_change_xpath", Constants.P_SYNC_HEADING);
    ExtentTestCase.getTest().info("P-synch Password Change Screen : Message : "
        + getElementText("secutrity_p_sync_text_xpath"));
    testElementAssert("secutrity_p_sync_link_xpath", Constants.PSYNCH_LINK);
    getElement("backbutton_acc_id").click();
    ExtentTestCase.getTest().info("User is navigating on My guides Screen");
  }

  /**
   * Click on 'Request for id ' link and verify screen heading and 'email held
   * desk' button of 'Request for id' screen.
   */
  public void requestForId() {
    getElements("tab_list_xpath").get(1).click();
    testElementAssert("request_id_heading_xpath", Constants.REQUEST_ID_HEADING);
    ExtentTestCase.getTest().info("'Request For Id' heading is displayed");
    testElementAssert("email_help_desk_xpath", Constants.EMAIL_HELP_DESK);
    ExtentTestCase.getTest().info("'EMAIL HELP DESK' button is displayed");
    getElement("backbutton_acc_id").click();
    ExtentTestCase.getTest().info("User is navigating on My guides Screen");
  }

  /**
   * click on 'guidelines' link and verify guidelines screen heading.
   */
  public void guidelineScreen() {
    getElements("tab_list_xpath").get(2).click();
    testElementAssert("security_p_sync_pws_change_xpath", Constants.GUIDELINES_HEADING);
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("guidelines_screen_message_xpath", "Guidelines",
          Constants.One_time_scroll);
      subTitles = getTabList("guidelines_screen_section_points_xpath", "Section Points", Constants.One_time_scroll);
    } else {
      titles = getListItems("guidelines_screen_message_xpath", "Guidelines");
    }
    ExtentTestCase.getTest().info("Guidelines Points are: ");
    for (String guideLinestext : titles) {
      ExtentTestCase.getTest().info(guideLinestext);
    }
    getElement("backbutton_acc_id").click();
    ExtentTestCase.getTest().info("User is on My guides Screen");
  }

  /**
   * Verify Benefits list: Oasis list and 'Quick IT help' list.
   */
  public void verifyBenefitsList() throws InterruptedException {
    navigateToMyGuideScreen("benefits_xpath", 2);
    // getElement("benefits_xpath").click();
    ExtentTestCase.getTest().info("'Oasis' Screen is displayed");
    OasisScreen();
    ExtentTestCase.getTest().info("'Quick It Help' Screen is displayed");
    quickItHelpScreen();
    ExtentTestCase.getTest().info("User is on My guides Screen and navigating to Dashboard");
    getElement("backbutton_acc_id").click();
    ExtentTestCase.getTest().info("Navigated to Dashboard Screen");
  }

  /**
   * click on Quick IT Help link and verify screen heading and text.
   */
  public void quickItHelpScreen() {
    getElements("tab_list_xpath").get(2).click();
    testElementAssert("quick_it_help_heading_xpath", Constants.QUICK_IT_HELP);
    ExtentTestCase.getTest().info("'Quick IT Help' heading is displayed");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("quick_it_help_text_xpath", "Quick It Help ", Constants.One_time_scroll);
      subTitles = getListItems("quick_subtitle_xpath", "Oasis List");
    } else {
      titles = getListItems("quick_it_help_text_xpath", "Oasis");
      subTitles = getListItems("quick_subtitle_xpath", "Oasis List");
    }
    ExtentTestCase.getTest().info("Quick IT Help : Contact online service desk by following modes");
    getElement("backbutton_acc_id").click();
    ExtentTestCase.getTest().info("User is on My guides Screen");
  }

  /**
   * click on Oasis link and verify screen heading and text.
   */
  public void OasisScreen() {
    getElements("tab_list_xpath").get(1).click();
    testElementAssert("oasis_heading_xpath", Constants.OASIS);
    ExtentTestCase.getTest().info("'Oasis' heading is displayed");
    ExtentTestCase.getTest().info("Oasis : Followings are the quick and real time Support : ");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("oasis_text_xpath", "Oasis", Constants.One_time_scroll);
      subTitles = getTabList("oasis_subtitle_xpath", "Oasis List",Constants.One_time_scroll);
    } else {
      titles = getListItems("oasis_text_xpath", "Oasis");
      subTitles = getListItems("oasis_subtitle_xpath", "Oasis List");
    }
    for (String oasisText : titles) {
      ExtentTestCase.getTest().info(oasisText);
    }
    getElement("backbutton_acc_id").click();
    ExtentTestCase.getTest().info("User is on My guides Screen");
  }

  /**
   * Verify Finance list : Emp stock purchase and filling travel. click on emp
   * stock purchase link and verify heading and its link .
   */
  public void verifyFinanceList() throws InterruptedException {
    String financeText;
    navigateToMyGuideScreen("finance_xpath", 1);
    financeText = getElements("tab_list_xpath").get(0).getText();
    Assert.assertEquals(financeText, Constants.EMP_STOCK_PURCHASE_PLAN);
    ExtentTestCase.getTest().info("'Employee Stock Purchase Plan' is displayed");
    financeText = getElements("tab_list_xpath").get(1).getText();
    Assert.assertEquals(financeText, Constants.FILLING_TRAVEL_EXPENSES);
    ExtentTestCase.getTest().info("'Filling Travel and Expenses' is displayed");
    getElements("tab_list_xpath").get(0).click();
    testElementAssert("emp_stock_purchase_plan_xpath", Constants.EMP_STOCK_PURCHASE_PLAN);
    ExtentTestCase.getTest().info("'Employee Stock Purchase Plan' heading is displayed ");
    ExtentTestCase.getTest().info(
        Constants.EMP_STOCK_PURCHASE_PLAN + " Message : " + getElementText("emp_stock_text_xpath"));
    testElementAssert("emp_stock_link_xpath", Constants.EMP_STOCK_LINK);
    ExtentTestCase.getTest().info("'" + Constants.EMP_STOCK_LINK + "'" + " link is displayed ");
    getElement("backbutton_acc_id").click();
    ExtentTestCase.getTest().info("User is on My guides Screen and navigating to Dashboard");
    getElement("backbutton_acc_id").click();
    ExtentTestCase.getTest().info("Navigated to Dashboard Screen");
  }

  /**
   * Get List of My Guides Screen.
   */
  public List<MobileElement> getTabList(String xpath, String tabName) {
    titlesName = getElements(xpath);
    for (int i = 0; i < titlesName.size(); i++) {
      ExtentTestCase.getTest().info(tabName + ": " + titlesName.get(i).getText());
      LOGGER.info(tabName + " list : {} ", titlesName.get(i).getText());
    }
    return titlesName;
  }
}
