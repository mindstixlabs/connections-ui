/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtentTestCase;

/**
 * Contains method of 'View Apps for me'Screen
 * 
 * @author Minstix
 *
 */
public class ViewAppsForMeScreen extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(ViewAppsForMeScreen.class);
  private List<String> titles = new ArrayList<String>();
  private List<String> subTitles = new ArrayList<String>();
  private int totalTitleNumber = 0;

  /**
   * Verify 'Apps For Me'Screen.
   */
  public void verifyAppsForMeScreen() throws InterruptedException {
    doneButtondisplayed();
    waitUntilVisiblityWithTime("manage_my_task_xpath");
    customVerticalSwipe(0.80, 0.20);
    clickElement("view_apps_xpath", "'View Apps form me' link");
    testElementAssert("apps_for_me_heading_xpath", "APPS FOR ME");
    ExtentTestCase.getTest().info("'APPS FOR ME' heading is displayed");
    testElementAssert("workspaceone_xpath", "WorkspaceONE");
    ExtentTestCase.getTest().info("'WorkspaceONE' label is displayed");
    String vmwareText = getElements("vmware_xpath").get(1).getText();
    LOGGER.info("Vmware Text : {} ", vmwareText);
    ExtentTestCase.getTest().info("'VMware' label is displayed");
  }

  /**
   * Verify Workspace One List.
   */
  public void verifyWorkspaceList() throws InterruptedException {
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("workspaceone_list_xpath", "Workspace One", Constants.size);
    } else {
      titles = getListItems("workspaceone_list_xpath", "Workspace One");
      subTitles = getListItems("workspace_subtitle_list_xpath", "Workspace one subtitles");
    }
    LOGGER.info("Workspace one list size : {} ", titles.size());
    totalTitleNumber = totalTitleNumber + titles.size();
  }

  /**
   * Verify Vmware List.
   */
  public void verifyVmwareList() throws InterruptedException {
    getElements("vmware_xpath").get(1).click();
    ExtentTestCase.getTest().info("Clicked on Vmware link");
    isElementDisplayed("vmware_xpath", "Vmware link");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("vmware_list_xpath", "Vmware", Constants.size);
    } else {
      titles = getListItems("vmware_list_xpath", "Vmware list Items");
      subTitles = getListItems("vmware_subtitles_xpath", "Vmware subtitles ");
    }
    LOGGER.info("Vmware list size : {} ", titles.size());
    totalTitleNumber = totalTitleNumber + titles.size();
    getElement("view_all_apps_xpath").click();
    ExtentTestCase.getTest()
        .info("Clicked on 'view all Apps' button and workspace one pop up is opened"
            + getElementText("workspace_popup_message_xpath"));   
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      waitUntilVisiblityWithTime("cancel_button_id");
      getElement("cancel_button_id").click(); 
    }else {
      waitUntilVisiblityWithTime("cancel_button_acc_id");
    getElement("cancel_button_acc_id").click();
    }
    ExtentTestCase.getTest().info("Clicked on Cancel Button");
    clickElement("backbutton_acc_id", "Back Button");
    ExtentTestCase.getTest().info("User is on dashboard screen");
  }

  /**
   * Get Total Apps in 'View Apps for me ' section. and verify it
   */
  public int verifyTotalApps() {
    swipeUp();
 //   totalTitleNumber = totalTitleNumber - 1;
    LOGGER.info("Total Number of apps : {} ", totalTitleNumber);
    String totalApps = getElementText("apps_count_xpath");
    ExtentTestCase.getTest().info("'view apps for me' String : " + totalApps);
    LOGGER.info("'view apps for me' String : {} ", totalApps);
    String count = totalApps.substring(9, 11);
    LOGGER.info("Total apps :{} ", count);
    int totalTitleNumberGuides = Integer.parseInt(count);
    ExtentTestCase.getTest().info("'view apps for me ' String : " + totalApps
        + " Total apps Count: " + totalTitleNumberGuides);
    Assert.assertEquals(totalTitleNumber, totalTitleNumberGuides);
    return totalTitleNumberGuides;
  }
}
