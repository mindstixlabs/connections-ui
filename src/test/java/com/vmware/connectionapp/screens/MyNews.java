/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.connectionapp.screens;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.vmware.connectionapp.api.ServiceProvider;
import com.vmware.connectionapp.setup.TestSuiteSetup;
import com.vmware.connectionapp.utils.Constants;
import com.vmware.connectionapp.utils.ExtentTestCase;
import com.vmware.connectionapp.utils.RestAssuredUtility;

import io.appium.java_client.MobileElement;

/**
 * Contains a method of my news screen
 * 
 * @author Mindstix
 */
public class MyNews extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(MyNews.class);
  private ServiceProvider serviceProvider = new ServiceProvider();
  private Response response;
  private List<String> titles = new ArrayList<String>();

  /**
   * My Update screen validation.
   * 
   * @throws InterruptedException
   */
  public void verifyUpdateScreenValidation() throws InterruptedException {
    doneButtondisplayed();
    ExtentTestCase.getTest().info("User is on dasboard and navigating to my news screen");
    waitUntilVisiblityWithTime("news_screen_heading_xpath");
    getElement("news_screen_heading_xpath").click();
    testElementAssert("my_news_heading_screen_xpath", "MY NEWS");
    ExtentTestCase.getTest().info("'MY NEWS' heading is displayed");
    testElementAssert("updated_xpath", "Updates");
    ExtentTestCase.getTest().info("'Updated' label is displayed");
    isElementDisplayed("updated_xpath", "News");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      checkNavigationOfUpdatesScreen();
      verifyNewsScreen();
    } else {
      checkNavigationOfUpdatesScreenForIOS();
      verifyNewsScreen();
    }
  }

  /**
   * Update Screen : To validate a navigation of all newsletter.
   */
  public void checkNavigationOfUpdatesScreen() {
    List<MobileElement> newsletterLink = getElements("newsletter_xpath");
    for (int i = 0; i < newsletterLink.size(); i++) {
      String newsletter = newsletterLink.get(i).getText();
      LOGGER.info(newsletter);
      ExtentTestCase.getTest().info("Weblinks : " + newsletter);
      newsletterLink.get(i).click();
      getElement("backbutton_acc_id").click();
    }
    getTabList("newsletter_xpath", "Update Screen : Weblinks ", Constants.size);
  }

  /**
   * News Screen : Validation of news screen.
   * 
   * @throws InterruptedException
   */
  public void verifyNewsScreen() throws InterruptedException {
  //  getElement("news_screen_heading_xpath").click();
    getElements("news_xpath").get(1).click();
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
    getTabList("announcement_xpath", "My News : Announcement", Constants.size);
    }
    else {
      getListItems("announcement_xpath", "My News : Announcement");
    }
    getElement("backbutton_acc_id").click();
  }

  /**
   * Verify Update Details from Api response.
   */
  public void verifyUpdateDetails() throws InterruptedException {
    doneButtondisplayed();
    response = serviceProvider.getUpdateDetails();
    JsonPath jp = RestAssuredUtility.getJsonPath(response);
    LOGGER.info("Extracting JSON");
    ExtentTestCase.getTest().info("User is on dasboard and navigating to my news screen");
    getElement("news_screen_heading_xpath").click();
    updateTitles(jp);
    getElement("backbutton_acc_id").click();
  }

  /**
   * Json Traversing for Update Titles on My News Screen.
   */
  public void updateTitles(JsonPath jsosForUpdateTitles) {
    waitUntilVisiblityWithTime("newsletter_xpath");
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("newsletter_xpath", "Update Screen : Weblinks ", Constants.size);
    } else {
      titles = getListItems("newsletter_xpath", "Update Screen : Weblinks");
    }
    LOGGER.info("UPDATE TITLES SIZE : {} ", titles.size());
    String title = jsosForUpdateTitles.getString("d.results[0].Title");
    LOGGER.info("Title : {} ", title);
    for (int i = 0; i < titles.size(); i++) {
      String jsonPathIndex = "d.results[" + i + "].Title";
      String updateTitleFromApi = jsosForUpdateTitles.getString(jsonPathIndex);
      String updateTitleFromUi = titles.get(i).toString();
      LOGGER.info("Update Titles from Api : {}  ", updateTitleFromApi);
      LOGGER.info("Update Titles from Ui : {} ", updateTitleFromUi);    
      if (!updateTitleFromUi.contains(Constants.TITLEVALUE)) {
        Assert.assertEquals(updateTitleFromApi, updateTitleFromUi);
        ExtentTestCase.getTest().info("Actual Updates Titles : " + updateTitleFromUi
            + " Expected Updated Titles : " + updateTitleFromApi);
      } else {
        LOGGER.info("Found June Title");
      }
    }
  }

  /**
   * Verify News details from api response.
   */
  public void verifyNewsDetails() throws InterruptedException {
    doneButtondisplayed();
    response = serviceProvider.getAnnouncementInfo();
    JsonPath jp = RestAssuredUtility.getJsonPath(response);
    ExtentTestCase.getTest().info("User is on dasboard and navigating to my news screen");
    getElement("news_screen_heading_xpath").click();
    getElements("news_xpath").get(1).click();
    newsTitles(jp);
    getElement("backbutton_acc_id").click();
  }

  /**
   * Json Traversing for News Titles on My News Screen.
   */
  public void newsTitles(JsonPath jsonForNewsTitles) {
    if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      titles = getTabList("announcement_xpath", "My News : Announcement", Constants.size);
    } else {
      titles = getListItems("announcement_xpath", "My News : Announcement ");
    }
    for (int i = 0; i < titles.size(); i++) {
      String jsonPathIndex = "d.results[" + i + "].Description";
      String newsTitleFromApi = jsonForNewsTitles.getString(jsonPathIndex);
      String newsTitleFromUi = titles.get(i).toString();
      LOGGER.info("News Titles from Api : {}  ", newsTitleFromApi);
      LOGGER.info("News Titles from Ui : {} ", newsTitleFromUi);
      // Assert.assertEquals(newsTitleFromApi, newsTitleFromUi);
      ExtentTestCase.getTest().info("Actual News Titles : " + newsTitleFromUi
          + " Expected News Titles : " + newsTitleFromApi);
      LOGGER.info("Actual News Titles : {} Expected News Titles : {} ", newsTitleFromUi,
          newsTitleFromApi);
    }
  }

  /**
   * Update Screen : To validate a navigation of all newsletter.
   */
  public void checkNavigationOfUpdatesScreenForIOS() {
    List<MobileElement> newsletterLink = getElements("newsletter_xpath");
    for (int i = 0; i < newsletterLink.size(); i++) {
        String newsletter = newsletterLink.get(i).getText();
        LOGGER.info(newsletter);
        ExtentTestCase.getTest().info("Weblinks : " + newsletter);
        newsletterLink.get(i).click();
        getElement("backbutton_acc_id").click();
        if (i==5) {
          LOGGER.info("RSA OCCURED");
          break;
        }
    }
  }
}
