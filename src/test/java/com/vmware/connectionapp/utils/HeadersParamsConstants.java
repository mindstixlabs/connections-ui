/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapp.utils;

/**
 * Setting parameters and headers.
 * 
 * @author Mindstix
 *
 */
public final class HeadersParamsConstants {
    public static final String SCOPE = "read";
    public static final String AUTHORIZATION = "Basic ZGlyZWN0X2FjY2Vzc19hcHA6djN3NHIzRDFyM2N0QWNjM3M1";
    public static final String AUTHORIZATION_ALEART = "Bearer ";
    public static final String CONTENT_TYPE = "application/json";
    public static final String ACCEPT = "application/json";
    public static final String REMOTE_USER_VALUE = "prashantj";
    public static final String WRONG_ACCEPT_VALUE = "AAA";
    public static final String WRONG_CONTENT_TYPE = "aaaa";
}
