/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapp.utils;

/**
 * This class contains the constants.
 * 
 * @author Mindstix.
 *
 */
public final class Constants {
  public static final String APPIUM_PORT = "4723";

  public static final String DOMAIN_NAME = "vmware.com";
  public static final String DROPDOWN_ID = "userStoreDomain";
  public static final String DROPDOWN_VALUE = "System Domain";
  public static final String NEXT_ID = "userStoreFormSubmit";
  public static final String ALLOW_PERMISSION = "Allow";
  public static final String DENY_PERMISSION = "Deny";

  public static final String USER_NAME = "c2hhcm1hYW5raXRh";
  public static final String USER_PASSWORD = "TWluZHN0aXhAMTIzNA==";
  public static final int size = 3;
  public static final int SIZE = 2;
  public static final int Loop_size = 4;
  public static final String TITLEVALUE = "28 Jun";
  public static final int One_time_scroll = 1;
  public static final String USER_NAME2 = "ZHZpbmNodXJrYXI=";
  public static final String USER_PASSWORD2 = "TWluZHN0aXhAMTIz";

  public static final String AUTHORIZATION = "Authorization";
  public static final String CONTENT_TYPE = "Content-Type";
  public static final String ACCEPT = "Accept";
  public static final String REMOTE_USER = "X-Remote-User";

  public static final String REDIRECT_URI = "redirect_uri";
  public static final int STATUSCODE_200 = 200;
  public static final String API_METHOD_GET = "GET";
  public static final String API_METHOD_POST = "POST";
  public static final String EMPTY_JSON_BODY = "{ }";

  public static final String PRINTER_LINK = "the Audio/Video instructions on 'How to connect to a network Printer'";
  public static final String P_SYNC_HEADING = "P-synch Password Change";
  public static final String REQUEST_ID_HEADING = "REQUEST FOR ID";
  public static final String EMAIL_HELP_DESK = "EMAIL HELP DESK";
  public static final String GUIDELINES_HEADING = "Guidelines";
  public static final String PSYNCH_LINK = "https://psynch.vmware.com";
  public static final String EMP_STOCK_PURCHASE_PLAN = "Employee Stock Purchase Plan";
  public static final String FILLING_TRAVEL_EXPENSES = "Filling Travel and Expenses";
  public static final String QUICK_IT_HELP = "Quick IT Help";
  public static final String OASIS = "Oasis";
  public static final String EMP_STOCK_LINK = "VMware Source > Groups > Finance and Operations > Stock";
  public static final String REQ_RSA_TOKEN="chevron Request RSA Token";
  public static final String[] DASHBOARD_LINK_NAMES = { "Manage My Tasks", "View Updates & News",
      "Search Guides for Help", "View Apps for Me" };
  
  public static final String[] MY_TASKS_LIST = { "Desk Phone Setup", "Printer Setup",
      "Audio Conferencing Setup", "Badge Received", "P-synch Password Change", "Request RSA Token",
      "Office 365", "Email Signature & VMware Template", "Skype for Business", "Workspace ONE",
      "Work/Home Contact Information", "HR Homepage on VMware Source", "My Goals" };
  
  public static final String OFFICE_365_LINK = "VMware Source > Apps & Tools > Workspace ONE > Office 365";
  public static final String EMAIL_SIGNATURE_LINK = "VMware Source > Apps & Tools > Workspace ONE > VM Vault > Sales Support > Groups: Global Marketing > Brand & Creative";
  public static final String SKYPE_BUISNESS_LINK = "VMware Source > IT > Collaboration > UCC Core";
  public static final String WORKSPACE_ONE_LINK="VMware Source > Apps & Tools > Workspace ONE";
  public static final String WORKDAY_LINK = "VMware Source > Groups > HR > Tools: Workday";
  public static final String HR_GROUP_LINK="VMware Source > Groups > HR";
  public static final String HAMBURGER_USERNAME="Ankita Sharma (c)";
  public static final String HAMBURGER_DESIGNATION="Mobile QE Engineer";
  public static final String HAMBUREGR_EXP_TEXT ="HOW WAS YOUR EXPERIENCE?";
  
}
