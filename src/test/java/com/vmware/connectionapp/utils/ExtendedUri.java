/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapp.utils;

/**
 * Setting extended URIs.
 * 
 * @author mindstix
 *
 */
public final class ExtendedUri {
    public static final String EXTENDED_LOGIN_URI = "/authorize?response_type=code&redirect_uri=http://mbei.vmware.com:8443/oauth-server/access_response&client_id=direct_access_app&idp=vidm";
    public static final String EXTENDED_ACCESS_TOKEN_URI_1 = "/token?code=";
    public static final String EXTENDED_ACCESS_TOKEN_URI_2 = "&redirect_uri=http://mbei.vmware.com:8443/oauth-server/access_response&grant_type=authorization_code";
    public static final String EXTENDED_GET_GLOBAL_COMM_URL = "/getGlobalCommunication";
    public static final String EXTENDED_GET_USER_DETAIL_URL="/getUserDetails?username=sharmaankita@vmware.com";
    public static final String EXTENDED_SEARCH_DETAILS_URL="/gsaKeySearchDetails?searchKey=take";
    public static final String EXTENDED_ANNOUNCEMENT_URL="/getAnnouncements?username=sharmaankita@vmware.com";
}